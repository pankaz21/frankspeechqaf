package com.johnstonhowse.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		
		features = "Feature", 
		glue = "com.johnstonhowse.steps", 
		tags = "",
		plugin = "pretty" 

)

public class TestRunner extends AbstractTestNGCucumberTests {

}
