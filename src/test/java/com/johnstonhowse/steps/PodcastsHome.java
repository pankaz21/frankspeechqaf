package com.johnstonhowse.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.johnstonhowse.common.JHBase;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PodcastsHome extends JHBase {
	
	
	
	@When("^I click podcasts and the podcasts home$")
	public void i_click_podcasts_and_the_podcasts_home() throws Throwable   {
		
		WebElement a = driver.findElement(By.xpath("//a[@class='dropdown-toggle active-trail']"));

		Actions ac = new Actions(driver);
		ac.moveToElement(a).build().perform();

		WebElement podcastHome = driver.findElement(By.xpath("(//div[@class='mega-dropdown-inner']//a)[1]"));
		Thread.sleep(2000);
		podcastHome.click();


	}

	@When("^I click on first category link$")
	public void i_click_on_first_category_link() {

	}

	@Then("^I see related videos$")
	public void i_see_related_videos(){

	}

}
