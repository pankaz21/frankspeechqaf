package com.johnstonhowse.steps;

import org.openqa.selenium.By;

import com.johnstonhowse.common.JHBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Fpi181tags extends JHBase {
	@Given("^I am in the user homepage$")
	public void i_am_in_the_user_homepage() throws Throwable {
		LaunchBrowser();
		   
	   
	}

	@When("^I click on the news Tab$")
	public void i_click_on_the_news_Tab() throws Throwable {
	 driver.findElement(By.xpath("//a[@class='we-mega-menu-li'][normalize-space()='News']")).click();   
	   
	}

	@When("^I click On one of the tags$")
	public void i_click_On_one_of_the_tags() throws Throwable {
	  driver.findElement(By.xpath("//a[@href='/topics/lindell']")) .click(); 
	   
	}

	@When("^I click on one of the author name$")
	public void i_click_on_one_of_the_author_name() throws Throwable {
	 driver.findElement(By.xpath("//article[@class='teaser--card teaser--large-card teaser']//a[normalize-space()='Amir George']")).click();   
	   
	}

	@Then("^I am able to see Tag related all section\\.$")
	public void i_am_able_to_see_Tag_related_all_section() throws Throwable {
	    
	   
	}



}
