package com.johnstonhowse.steps;

import org.openqa.selenium.By;

import com.johnstonhowse.common.JHBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Fpi181latestnews extends JHBase {
	@Given("^I am on the site$")
	public void i_am_on_the_site() throws Throwable {
		LaunchBrowser();
	    
	}

	@When("^I click on the NEWS$")
	public void i_click_on_the_NEWS() throws Throwable {
		driver.findElement(By.xpath("//a[@class='we-mega-menu-li'][normalize-space()='News']")).click() ;  
	    
	}

	@When("^I click On one of the LATEST NEWS$")
	public void i_click_On_one_of_the_LATEST_NEWS() throws Throwable {
	   driver.findElement(By.xpath("//span[normalize-space()='Will Johnson on The Todd Coconato Show']")).click();
	    
	}

	@When("^I click on one of the author name or link or news$")
	public void i_click_on_one_of_the_author_name_or_link_or_news() throws Throwable {
	   driver.findElement(By.xpath("//span[@class='byline__author-name']//a[normalize-space()='Todd Coconato']")).click();
	    
	}

	@Then("^I am able to see LATEST NEWS related all sections like trending stories,tags,etc$")
	public void i_am_able_to_see_LATEST_NEWS_related_all_sections_like_trending_stories_tags_etc() throws Throwable {
	   
	    
	}

}
