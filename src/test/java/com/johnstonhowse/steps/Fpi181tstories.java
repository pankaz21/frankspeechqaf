package com.johnstonhowse.steps;

import org.openqa.selenium.By;

import com.johnstonhowse.common.JHBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Fpi181tstories extends JHBase {
	@Given("^User am on the site$")
	public void user_am_on_the_site() throws Throwable {
		LaunchBrowser();  
	  
	}

	@When("^User click on the NEWS Tab$")
	public void user_click_on_the_NEWS_Tab() throws Throwable {
	 driver.findElement(By.xpath("//a[@class='we-mega-menu-li'][normalize-space()='News']")) .click();  
	  
	}

	@When("^User click On one of the Trending Stories$")
	public void user_click_On_one_of_the_Trending_Stories() throws Throwable {
	 driver.findElement(By.xpath("//span[contains(text(),'Radar Footage of 9 UFOs Swarming US Navy Ship Conf')]")).click();   
	  
	}

	@When("^User click on one of the author name$")
	public void user_click_on_one_of_the_author_name() throws Throwable {
	 driver.findElement(By.xpath("//div[@class='themag-layout themag-layout--twocol-section themag-layout--twocol-section--custom themag-layout--py-xsmall themag-layout--my-default shadow-sm']//span[1]//a[1]")).click();   
	  
	}

	@Then("^User able to see Trending Stories related all sections like trending stories,tags,etc$")
	public void user_able_to_see_Trending_Stories_related_all_sections_like_trending_stories_tags_etc() throws Throwable {
	    
	  
	}


}
