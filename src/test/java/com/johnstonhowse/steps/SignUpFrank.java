package com.johnstonhowse.steps;

import org.openqa.selenium.By;

import com.johnstonhowse.common.JHBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SignUpFrank extends JHBase {
	
	@Given("^I'm on the frank speech home page$")
	public void i_m_on_the_frank_speech_home_page() throws Throwable {
		LaunchBrowser();
	 
	}

	@When("^I click on sign up for frank$")
	public void i_click_on_sign_up_for_frank() throws Throwable {
	 driver.findElement(By.xpath("//*[@id=\"edit-username-login\"]/div/a")).click();
	 
	}

	@When("^Enter valid phone number$")
	public void enter_valid_phone_number() throws Throwable {
	driver.findElement(By.xpath("//*[@id=\"query_phone\"]")).sendKeys("5168557411"); 
	 
	}

	@When("^Enter valid email address$")
	public void enter_valid_email_address() throws Throwable {
	driver.findElement(By.xpath("//*[@id=\"edit-mail\"]")).sendKeys("pankaztalukdar@gmail.com");   
	 
	}

	@When("^Enter user name$")
	public void enter_user_name() throws Throwable {
	 driver.findElement(By.xpath("//*[@id=\"edit-name\"]")).sendKeys("frank_21");  
	 
	}

	@When("^Enter Password$")
	public void enter_Password() throws Throwable {
	driver.findElement(By.xpath("//*[@id=\"edit-pass-pass1\"]")).sendKeys("Frank@2021");   
	 
	}

	@When("^Confirm password again$")
	public void confirm_password_again() throws Throwable {
	 driver.findElement(By.xpath("//*[@id=\"edit-pass-pass2\"]")).sendKeys("Frank@2021");  
	 
	}

	@When("^Enter Captcha Image$")
	public void enter_Captcha_Image() throws Throwable {
	driver.findElement(By.xpath("//*[@id=\"edit-captcha-response\"]")).sendKeys("A23bC3D");   
	 
	}

	@When("^Click on Create Account$")
	public void click_on_Create_Account() throws Throwable {
	   
	 
	}

	@Then("^I can sign up account created successfully$")
	public void i_can_sign_up_account_created_successfully() throws Throwable {
	 //driver.findElement(By.xpath("//*[@id=\"edit-submit\"]")).click();  
	 
	}

	
	

}
