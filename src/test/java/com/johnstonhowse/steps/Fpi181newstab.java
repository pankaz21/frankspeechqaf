package com.johnstonhowse.steps;

import org.openqa.selenium.By;

import com.johnstonhowse.common.JHBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Fpi181newstab extends JHBase{
	@Given("^I am on the home page$")
	public void i_am_on_the_home_page() throws Throwable {
		LaunchBrowser();
	   
	}

	@When("^I click on the News tab$")
	public void i_click_on_the_News_tab() throws Throwable {
	 driver.findElement(By.xpath("//a[@class='we-mega-menu-li'][normalize-space()='News']")).click() ;  
	   
	}

	@Then("^I am able to see Latest headline news,latest news,tags,news,trending stories,popular topics etc\\.$")
	public void i_am_able_to_see_Latest_headline_news_latest_news_tags_news_trending_stories_popular_topics_etc() throws Throwable {
	    
	   
	}



}
