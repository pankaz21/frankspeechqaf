package com.johnstonhowse.steps;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.johnstonhowse.common.JHBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FrankNewsLink extends JHBase {

	String url = "";
	HttpURLConnection huc = null;
	int respCode = 200;
	
	
	//HomePage = hp;

	@Given("^I am on Frank freedom of speech homepage$")
	public void i_am_on_Frank_freedom_of_speech_homepage() {

		LaunchBrowser();

	}

	@When("^I click Frank News page$")
	public void i_click_podcasts_and_the_podcasts_home() {


		WebElement frankNewsLink = driver.findElement(By.xpath("//*[@id=\"tb-megamenu-main\"]/div/ul/li[4]/a"));	
		//hp = new HomePage(driver);
		frankNewsLink.click();
		

	}

	@When("^I check each link and click on the active links$")
	public void i_click_on_first_category_link() {

		List<WebElement> links = driver.findElements(By.tagName("a"));

		Iterator<WebElement> allLinks = links.iterator();

		while (allLinks.hasNext()) {

			url = allLinks.next().getAttribute("href");

			System.out.println(url);

			if (url == null || url.isEmpty()) {
				System.out.println("URL is not configured or it is empty");
				continue;
			}

			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());

				huc.connect();

				respCode = huc.getResponseCode();

				if (respCode == 400) {
					System.out.println(respCode + "    is a broken link  " + url);

				} else if (respCode > 400) {
					System.out.println(respCode + "    is Forbidden " + url);

				} else {
					//System.out.println(respCode + "     is a active link   " + url);	
					driver.navigate().to(url);
					break;
				}

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Then("^I close the browser$")
	public void i_find_all_broken_or_forbidded() {
		
		//driver.close();

	}

}
